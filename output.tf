#Membuat sebuah Output agar LoadBalancer IP keluar ketika selesai di apply.
output "load_balancer_ip" {
  value = aws_lb.default.dns_name
}