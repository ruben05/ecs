resource "aws_ecs_service" "hello_world" {
  name            = "hello-world-service"
  cluster         = local.cluster_arn
  task_definition = local.task_def
  desired_count   = local.task
  launch_type     = local.type_ecs

  network_configuration {
    security_groups = local.security_group_ecs
    subnets         = local.subnet_private_ecs
  }

  load_balancer {
    target_group_arn = local.target_group_ecs
    container_name   = "hello-world"
    container_port   = local.port_app
  }
}
