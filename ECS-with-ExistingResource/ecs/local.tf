locals {
    cluster_arn = "arn:aws:ecs:ap-southeast-1:360760555806:cluster/exp-cluster"
    task_def = "arn:aws:ecs:ap-southeast-1:360760555806:task-definition/hello-world-app:2"
    task = "2"
    security_group_ecs = ["sg-023a4ffaa6bbb2580"]
    subnet_private_ecs = ["subnet-092fe2c5678cb09d0","subnet-097bff13db8f19d20",] 
    target_group_ecs = "arn:aws:elasticloadbalancing:ap-southeast-1:360760555806:targetgroup/example-target-group/1c19c457662d0421"
    port_app = "3000"
    type_ecs = "FARGATE"
    }