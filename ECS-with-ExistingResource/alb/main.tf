provider "aws" {
  version = "~> 3.0"
}

resource "aws_lb" "default" {
  name            = "example-lb"
  subnets         = local.subnet_public
  security_groups = local.security_group
}

resource "aws_lb_target_group" "hello_world" {
  name        = "example-target-group"
  port        = 3000
  protocol    = "HTTP"
  vpc_id      = local.vpc_arn
  target_type = "ip"
}

resource "aws_lb_listener" "hello_world" {
  load_balancer_arn = aws_lb.default.id
  port              = local.port_http
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.hello_world.id
    type             = "forward"
  }
}