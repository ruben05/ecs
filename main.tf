terraform {
    backend "s3" {
        bucket = "finacceldev-vanjul-dev"
        key    = "playground/ECS-2/state.tfstate"
    }
}

#Kita Menentukan Region
provider "aws" {
  region = "ap-southeast-2"
}

#Membuat VPC baru dengan nama my_vpc 
resource "aws_vpc" "my_vpc" {
  cidr_block = "10.32.0.0/16"
}


#Kita Set Publik dan Private Subnet
resource "aws_subnet" "private_a" {
  cidr_block        = "10.32.0.0/24"
  availability_zone = "ap-southeast-2a"
  vpc_id            = aws_vpc.my_vpc.id
}

resource "aws_subnet" "private_b" {
  cidr_block        = "10.32.1.0/24"
  availability_zone = "ap-southeast-2b"
  vpc_id            = aws_vpc.my_vpc.id
}

resource "aws_subnet" "public_a" {
  cidr_block              = "10.32.2.0/24"
  availability_zone       = "ap-southeast-2a"
  vpc_id                  = aws_vpc.my_vpc.id
  map_public_ip_on_launch = true
}

resource "aws_subnet" "public_b" {
  cidr_block              = "10.32.3.0/24"
  availability_zone       = "ap-southeast-2b"
  vpc_id                  = aws_vpc.my_vpc.id
  map_public_ip_on_launch = true
}


#Kita membuat Internet Gateway agar VPC bisa berkomunikasi dengan Internet ( overinternet)
resource "aws_internet_gateway" "gateway" {
    vpc_id = aws_vpc.my_vpc.id
}

resource "aws_route" "internet_access" {
  route_table_id         = aws_vpc.my_vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gateway.id
}
#Kita memakain EIP untuk track alamat IP yang sedang berjalan di dalam aplikasi
resource "aws_eip" "gateway" {
  vpc        = true
  depends_on = [aws_internet_gateway.gateway]
}

#Set NAT gateway agar memudahkan terhubung ke internet dengan subnet publik di VPC kita
resource "aws_nat_gateway" "gateway" {
  subnet_id     = aws_subnet.public_a.id
  allocation_id = aws_eip.gateway.id
}

#Kita Set Route Table nya. Route Table adalah Diana kita menentukan network traffic dari VPC diarahkan
resource "aws_route_table" "private" {
  vpc_id = aws_vpc.my_vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.gateway.id
  }
}

resource "aws_route_table_association" "private" {
  subnet_id      = aws_subnet.private_a.id
  route_table_id = aws_route_table.private.id
}

#Security Group untuk kontrol Inbound dan Outbound , Untuk Security yang pertama kita set allow port 80
resource "aws_security_group" "lb" {
  name        = "example-alb-security-group"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
#Kita membuat sebuah Load Balancer dengan tiper ALB
resource "aws_lb" "default" {
  name            = "example-lb"
  subnets         = [
    aws_subnet.public_a.id,
    aws_subnet.public_b.id,
  ]
  security_groups = [aws_security_group.lb.id]
}
#Target Group dengan register port 80
resource "aws_lb_target_group" "hello_world" {
  name        = "example-target-group"
  port        = 80
  protocol    = "HTTP"
  vpc_id      = aws_vpc.my_vpc.id
  target_type = "ip"
}

#Listeners untuk foward ke target group yang di atas
resource "aws_lb_listener" "hello_world" {
  load_balancer_arn = aws_lb.default.id
  port              = "80"
  protocol          = "HTTP"

  default_action {
    target_group_arn = aws_lb_target_group.hello_world.id
    type             = "forward"
  }
}

#ECS - Task Definition
resource "aws_ecs_task_definition" "hello_world" {
  family                   = "hello-world-app"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 2048
#Kita mengambil Image node js nya dari dockerhub.
  container_definitions = <<DEFINITION
[
  {
    "image": "heroku/nodejs-hello-world",
    "cpu": 1024,
    "memory": 2048,
    "name": "hello-world-app",
    "networkMode": "awsvpc",
    "portMappings": [
      {
        "containerPort": 3000,
        "hostPort": 3000
      }
    ]
  }
]
DEFINITION
}

#Kita membuat Security group 1 lagi untuk expose port 3000 untuk keperluan node js nya.
resource "aws_security_group" "hello_world_task" {
  name        = "example-task-security-group"
  vpc_id      = aws_vpc.my_vpc.id

  ingress {
    protocol        = "tcp"
    from_port       = 3000
    to_port         = 3000
    security_groups = [aws_security_group.lb.id]
  }

  egress {
    protocol    = "-1"
    from_port   = 0
    to_port     = 0
    cidr_blocks = ["0.0.0.0/0"]
  }
  }

#Kita membuat sebuah Cluster baru dengan nama experiment-cluster
  resource "aws_ecs_cluster" "main" {
  name = "experiment-cluster"
}

#Membuat ECS Service dangan spesifikasi berikut.
resource "aws_ecs_service" "hello_world" {
  name            = "hello-world-service"
  cluster         = aws_ecs_cluster.main.id
  task_definition = aws_ecs_task_definition.hello_world.arn
  desired_count   =  2
  launch_type     = "FARGATE"

  network_configuration {
    security_groups = [aws_security_group.hello_world_task.id]
    subnets         = [aws_subnet.private_a.id, aws_subnet.private_b.id,]
  }

  load_balancer {
    target_group_arn = aws_lb_target_group.hello_world.id
    container_name   = "hello-world-app"
    container_port   = 3000
  }

  depends_on = [aws_lb_listener.hello_world]
}
